﻿namespace Domain.Enums;

public enum MediaType
{
    Contract,
    INN,
    EGRIP,
    OGRN,
    OGRNIP
}