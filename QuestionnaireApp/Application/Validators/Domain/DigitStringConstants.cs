﻿namespace Application.Validators.Domain;

public static class DigitStringConstants
{
    public static int INNLength = 10;
    public static int BIKLength = 9;
    public static int OGRNIPLength = 15;
    public static int OGRNLength = 13;
    public static int CheckingAccountLength = 20;
    public static int CorrespondentAccountLength = 20;
}