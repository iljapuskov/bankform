﻿namespace Application.DTOs.FinancialCredential;

public class FinancialCredentialByBIKDto
{
    public string CorrespondentAccount { get; set; }
    public string BankName { get; set; }
}