﻿namespace Application.DTOs.LimitedLiabilityCompany;

public class LimitedLiabilityCompanyByINNDto
{
    public string LongName { get; set; }
    public string ShortName { get; set; }
    public string OGRN { get; set; }
    public DateTime RegisterDate { get; set; }
}