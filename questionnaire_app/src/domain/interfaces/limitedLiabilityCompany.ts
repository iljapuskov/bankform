export interface LimitedLiabilityCompany {
    longName: string
    shortName: string
    ogrn: string
    registerDate: Date
}