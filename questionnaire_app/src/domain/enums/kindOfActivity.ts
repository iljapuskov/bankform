export enum KindOfActivity {
    IndividualEntrepreneur,
    LimitedLiabilityCompany
}